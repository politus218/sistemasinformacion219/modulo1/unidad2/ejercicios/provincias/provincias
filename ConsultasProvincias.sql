﻿-- Ejercicio 1. Selección de las provincias.

SELECT p.provincia FROM provincias p;

-- Ejercicio 2. Resultado de 2 + 3.

SELECT 2+3;

-- Ejercicio 3. Sacar las densidades de población de las provincias.

SELECT provincia, p.poblacion / p.superficie FROM provincias p;

-- Ejercicio 4. Nombre de las provincias de cada autonomía.

  SELECT autonomia,provincia FROM provincias;

-- Ejercicio 5. Cuanto vale la raiz cuadrada de 2.

  SELECT SQRT(2);

-- Ejercicio 6. ¿Cuantos caracteres tiene cada nombre de provincia?

  SELECT p.provincia,CHAR_LENGTH(p.provincia) FROM provincias p;

 -- Ejercicio 7. Listado de autonomías.

SELECT DISTINCT p.autonomia FROM provincias p;

-- Ejercicio 8. Provincias con el mismo nombre que su autonomía.

SELECT autonomia FROM provincias p
WHERE p.provincia = p.autonomia;

-- Ejercicio 9. Provincias que contienen el diptongo 'ue'

SELECT p.provincia FROM provincias p
WHERE p.provincia LIKE '%ue%';

-- Ejercicio 10. Provincias que empiezan por A.

SELECT p.provincia FROM provincias p
WHERE p.provincia LIKE 'a%';

-- Ejercicio 11. ¿Qué provincias están en autonomías con nombre compuesto?

SELECT p.provincia FROM provincias p
WHERE p.autonomia LIKE '% %';

-- Ejercicio 12 ¿Qué provincias tienen nombre compuesto?

  SELECT p.provincia FROM provincias p
  WHERE p.provincia LIKE '% %';

-- Ejercicio 13. ¿Qué provincias tienen nombre simple?

  SELECT p.provincia FROM provincias p
  WHERE p.provincia NOT LIKE '% %';

  -- Ejercicio 14. Muestra las provincias de Galicia, indicando si es Grande, Mediana o Pequeña en función de si su población supera el umbral de un millón o de medio millón de habitantes.

  SELECT p.provincia, CASE
  WHEN p.poblacion>1e6 THEN 'Grande'
  WHEN p.poblacion>5e5 THEN 'Mediana'
  ELSE 'Pequeña'
  END tamaño
  FROM provincias p
  WHERE p.autonomia='Galicia';

-- Ejercicio 15. Autonomías terminadas en 'ana'.

  SELECT DISTINCT p.autonomia FROM provincias p
  WHERE p.autonomia LIKE '%ana';

-- Ejercicio 16. ¿Cuántos caracteres tiene cada nombre de comunidad autónoma? Ordena el resultado por el nombre de la autonomía de forma descendente.

  SELECT DISTINCT p.autonomia,CHAR_LENGTH(p.autonomia) FROM provincias p
  ORDER BY p.autonomia DESC;

-- Ejercicio 17. ¿Qué autonomías tienen provincias de más de un millón de habitantes? Ordénalas alfabéticamente.

  SELECT DISTINCT p.autonomia FROM provincias p
  WHERE p.poblacion>1e6
  ORDER BY p.autonomia;

-- Ejercicio 18. Población del país.

  SELECT SUM(p.poblacion) FROM provincias p;

-- Ejercicio 19.  ¿Cuántas provincias hay en la tabla?

  SELECT COUNT(p.provincia) FROM provincias p;

-- Ejercicio 20. ¿Qué comunidades autónomas contienen el nombre de una de sus provincias? 

  SELECT p.autonomia,p.provincia,LOCATE(provincia,p.autonomia) FROM provincias p;

  SELECT autonomia FROM provincias WHERE LOCATE(provincia,autonomia)>0;

-- Ejercicio 21. ¿Qué autonomías tienen nombre compuesto? Ordena el resultado alfabéticamente en orden inverso.

  SELECT DISTINCT p.autonomia FROM provincias p
  WHERE p.autonomia LIKE '% %'
  ORDER BY p.autonomia DESC;

-- Ejercicio 22. ¿Qué autonomías tienen nombre simple? Ordena el resultado alfabéticamente en orden inverso.

  SELECT DISTINCT p.autonomia FROM provincias p
  WHERE p.autonomia NOT LIKE '% %'
  ORDER BY p.autonomia DESC;

-- Ejercicio 23. ¿Qué autonomías tienen provincias con nombre compuesto? Ordenar el resultado alfabéticamente.

  SELECT DISTINCT p.autonomia FROM provincias p
  WHERE p.provincia LIKE '% %'
  ORDER BY p.autonomia;

-- Ejercicio 24. Autonomías que comiencen por 'can' ordenadas alfabéticamente.

  SELECT DISTINCT p.autonomia FROM provincias p
  WHERE p.autonomia LIKE 'can%'
  ORDER BY p.autonomia;

-- Ejercicio 25. Listado de provincias y autonomías que contengan la letra ñ.

  SELECT p.provincia FROM provincias p
  WHERE p.provincia LIKE '%ñ%' COLLATE utf8_bin
  UNION
  SELECT p.autonomia FROM provincias p
  WHERE p.autonomia LIKE '%ñ%' COLLATE utf8_bin;
  

-- Ejercicio 26. Superficie del país.

  SELECT SUM(p.superficie) FROM provincias p;

-- Ejercicio 27. En un listado alfabético, ¿qué provincia estaría la primera?

  SELECT MIN(provincia) FROM provincias p;

-- Ejercicio 28. ¿Qué provincias tienen un nombre más largo que el de su autonomía?

  SELECT p.provincia FROM provincias p
  WHERE CHAR_LENGTH(p.provincia)>CHAR_LENGTH(p.autonomia);

-- Ejercicio 29. ¿Cuantas comunidades autónomas hay?

  SELECT COUNT(DISTINCT p.autonomia) FROM provincias p;

-- Ejercicio 30. ¿Cuánto mide el nombre de provincia más largo?

  SELECT MAX(CHAR_LENGTH (provincia)) FROM provincias p;

-- Ejercicio 31. ¿Cuánto mide el nombre de autonomía más corto?

  SELECT MIN(char_length (autonomia)) FROM provincias;

-- Ejercicio 32. Población media de las provincias entre 2 y 3 millones de habitantes sin decimales.

  SELECT round(AVG(poblacion)) FROM provincias
  WHERE poblacion BETWEEN 2e6 AND 3e6;

-- Ejercicio 33. Listado de autonomías cuyas provincias lleven alguna tilde en su nombre.

  SELECT DISTINCT autonomia FROM provincias
  WHERE lower(provincia) LIKE '%á%' COLLATE utf8_bin OR
        lower(provincia) LIKE '%é%' COLLATE utf8_bin OR
        lower(provincia) LIKE '%í%' COLLATE utf8_bin OR
        lower(provincia) LIKE '%ó%' COLLATE utf8_bin OR
        lower(provincia) LIKE '%ú%' COLLATE utf8_bin;

-- Ejercicio 34. Provincia más poblada.

  SELECT provincia FROM provincias
  WHERE poblacion =(SELECT MAX(poblacion) FROM provincias);

-- Ejercicio 35. Provincia más poblada de las inferiores a 1 millón de habitantes.

  SELECT provincia FROM provincias
  WHERE poblacion =(SELECT MAX(poblacion) FROM provincias
  WHERE poblacion<1e6);

-- Ejercicio 36. Provincia menos poblada de las superiores al millón de habitantes.

  SELECT provincia FROM provincias
  WHERE poblacion =(SELECT MIN(poblacion) FROM provincias
  WHERE poblacion>1e6);

-- Ejercicio 37. ¿En qué autonomía está la provincia más extensa? 

  SELECT autonomia FROM provincias
  WHERE superficie =(SELECT MAX(superficie) FROM provincias);

-- Ejercicio 38. ¿Qué provincias tienen una población por encima de la media nacional?

  SELECT provincia FROM provincias
  WHERE poblacion> (SELECT AVG(poblacion) FROM provincias); 

-- Ejercicio 39. Densidad de población del país.

  SELECT (46e6)/(5e5);

  SELECT (
    SELECT SUM(poblacion) FROM provincias
  )/(
    SELECT SUM(superficie) FROM provincias
  );

  SELECT SUM(poblacion)/SUM(superficie) FROM provincias;

-- Ejercicio 40. ¿Cuántas provincias tiene cada comunidad autónoma?

  SELECT autonomia,COUNT(provincia) FROM provincias
  GROUP BY autonomia;

-- Ejercicio 41. Obtén el listado de autonomías (una línea por autonomía), junto al listado de sus provincias en una única celda. Recuerda que, tras una coma, debería haber un espacio en blanco.
 
  SELECT autonomia,GROUP_CONCAT(provincia SEPARATOR ', ') FROM provincias GROUP BY autonomia;

-- Ejercicio 42. Listado del número de provincias por autonomía ordenadas de más a menos provincias y por autonomía en caso de coincidir

  SELECT COUNT(provincia) n,autonomia  FROM provincias
  GROUP BY autonomia ORDER BY n DESC, autonomia;

-- Ejercicio 43. ¿Cuántas provincias con nombre compuesto tiene cada comunidad autónoma?

  SELECT DISTINCT provincia, autonomia FROM provincias
  WHERE provincia like '% %'
  ORDER BY autonomia;

-- Ejercicio 44. ¿Cuántas provincias con nombre compuesto tiene cada comunidad autónoma?

  SELECT autonomia,COUNT(*) FROM provincias
  WHERE provincia like '% %'
    GROUP BY  autonomia
  ORDER BY autonomia;

  SELECT autonomia,COUNT(*) FROM provincias
  WHERE provincia LIKE '% %'
  GROUP BY autonomia;

-- Ejercicio 45. Autonomías uniprovinciales.

  SELECT * FROM provincias
  WHERE provincia = autonomia;

  SELECT autonomia FROM provincias
    GROUP BY autonomia
    HAVING COUNT(*)=1;

-- Ejercicio 46. ¿Qué autonomía tiene 5 provincias?

  SELECT autonomia FROM provincias
    GROUP BY autonomia
    HAVING COUNT(*)=5;

-- Ejercicio 47. Población de la autonomía más poblada.

  SELECT SUM(poblacion) FROM provincias
  WHERE autonomia ='Andalucia';

  SELECT MAX(poblacion) FROM
    ( SELECT SUM(poblacion) poblacion FROM provincias
  GROUP BY autonomia) c1;
 
-- Ejercicio 48. ¿Qué porcentaje del total nacional representa Cantabria en población y en superficie?

  SELECT poblacion,superficie FROM provincias
  WHERE autonomia ='Cantabria';

  SELECT SUM(poblacion) FROM provincias;

  SELECT SUM(superficie) FROM provincias;

  SELECT (SUM(poblacion)/SUM(superficie))/100 FROM provincias;

-- Ejercicio 49. Autonomía más extensa.

  SELECT SUM(superficie) FROM provincias
  WHERE autonomia ='Castilla y León';

SELECT MAX(superficie) FROM
    ( SELECT SUM(superficie) superficie FROM provincias
  GROUP BY autonomia) c1;


  

